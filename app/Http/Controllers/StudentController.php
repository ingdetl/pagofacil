<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id){

        $result = DB::table('t_alumnos')
            ->join('t_calificaciones', 't_alumnos.id_t_usuarios', '=', 't_calificaciones.id_t_usuarios')
            ->join('t_materias', 't_calificaciones.id_t_materias', '=', 't_materias.id_t_materias')
            ->where('t_alumnos.id_t_usuarios', $id)
            ->select(
                't_alumnos.id_t_usuarios',
                't_alumnos.nombre',
                't_alumnos.ap_paterno',
                't_materias.nombre as materia',
                't_calificaciones.calificacion',
                DB::raw('DATE_FORMAT(t_calificaciones.fecha_registro, "%d/%m/%Y") as fecha_registro')
            )
            ->get();

        $qualification = 0;
        foreach ($result as $data) {
            $qualification += $data->calificacion;
        }

        $average = $qualification / (count($result));

        return response()->json(array_add($result,'promedio',$average));

    }
}
